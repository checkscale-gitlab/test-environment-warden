# test-guard

Simulazione di un ambiente di test con redirect dinamico delle richieste.
Il sistema riceve in ingresso richieste HTTP e le instrada al correto server in base all'indirizzo IP di proveninza.
Per simulare richieste provenienti da IP differenti sono presenti dei proxy al bordo del sistema che intercettano le chiamate e le redirigono al reale server d'ingresso del sistema.

## Running
```sh
    docker-compose up -d
```

test-guard crea una rete virtuale accessibile dall'host sulla porta HTTP(80) tramite i proxy di simulazione dei partner.
Inoltre tramite l'esecuzionen di comandi docker è possibile modificare l'algoritmo di routing.

![Alt text](schemas/test-schema.svg)

## Using

### Host Machine → Containers

per poter accedere accedere ai proxy tramite browser è necessario configurare il file hosts dell'host emulando il DNS

```sh
    sudo echo "127.0.0.1 partner1.internal.lcl partner2.internal.lcl partner3.internal.lcl partner4.internal.lcl partner5.internal.lcl" >> /etc/hosts
```

### Verifica indirizzi IP assegnati ai partner
per verificare gli indirizzi IP assegnati ai singolo partner
```sh
    docker logs dns-tiro
```
## Routing

L'algoritmo di routing prevede l'instradamento delle richieste in base all'indirizzo IP del chiamante.
Se nessuna regola di routing è definita per un determinato IP la richiesta è inoltrata all'environment di default

### Gestione Routing

#### Aggiunta regola 

```sh
    docker exec -it test /etc/apache2/scripts/route-partner.sh IP-REQ TEST-ENV reload 
```
* `IP-REQ`: IP di provenienza della richiesta
* `TEST-ENV`: ambiente di test a cui redirigere la richiesta

Esempio:
```sh
    docker exec -it test /etc/apache2/scripts/route-partner.sh 172.18.238.20 test3 reload 
```
#### Visualizzazione regole attive

```sh
    docker exec -it test /etc/apache2/scripts/get-active-routes.sh 
```
 
#### Eliminazione Regola 

```sh
    docker exec -it test /etc/apache2/scripts/delete-rule.sh IP-REQ reload 
```
* `IP-REQ`: IP di provenienza della richiesta

Esempio:
```sh
    docker exec -it test /etc/apache2/scripts/delete-rule.sh 172.18.238.20 reload 
```

