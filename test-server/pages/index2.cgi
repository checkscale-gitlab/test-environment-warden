#!/usr/bin/perl -wT
use warnings FATAL => 'all';
use strict;
use CGI;
print CGI::header();
print "<h1>", $ENV{'SERVER_NAME'}, "</h1>", "\n";
print "Client IP:      ", $ENV{'REMOTE_ADDR'}, "<BR>", "\n";

#foreach my $key (sort(keys(%ENV))) {
#    print "$key = $ENV{$key}<br>\n";
#}


1;
